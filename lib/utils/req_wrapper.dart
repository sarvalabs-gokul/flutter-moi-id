import 'dart:convert';
import 'package:http/http.dart' as http;

const baseURL = "https://devapi.moinet.io/moi-id";

Uri parseURL(String routePath, String routeName) {
  List<String> urlComponents = [baseURL, routePath, routeName];
  return Uri.parse(urlComponents.join("/"));
}

Future<http.Response> sendRequest(
    bool isGetRequest, String routePath, String routeName, Object reqBody) {
  Uri urlParsed = parseURL(routePath, routeName);

  if (isGetRequest) {
    return http.get(urlParsed);
  } else {
    return http.post(
      urlParsed,
      headers: {'content-type': 'application/json'},
      body: jsonEncode(reqBody),
    );
  }
}
