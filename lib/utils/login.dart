import 'req_wrapper.dart';

const invalidAddress = "0x0000000000000000000000000000000000000000";

Future<String> getUserID(String uN) async {
  var userNameResponse = await sendRequest(
      true, 'identity', 'getdefaultaddress?username=9Xv4yoVW7ak6qhG7yL', Null);
  if (userNameResponse.statusCode == 200) {
    if (userNameResponse.body == invalidAddress) {
      throw "invalid username";
    }
    return userNameResponse.body;
  }
  throw userNameResponse.body;
}

Future<Object> getZkProofOfUser(String userID, String proofType) async {
  var getZkProof = await sendRequest(false, 'auth', 'getmks?username',
      <String, String>{'defAddr': userID, 'typeOfProof': proofType});
  if (getZkProof.statusCode == 200) {
    return Future<Object>.value(getZkProof.body);
  }
  throw getZkProof.body;
}
