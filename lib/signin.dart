import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import "utils/login.dart";

typedef ValueChanged2Params<S, B> = void Function(S value1, B value2);
typedef ValueChanged<T> = void Function(T value);

class SignIn extends StatefulWidget {
  final Color primaryColor, secondaryColor, textColor;

  final ValueChanged2Params<String, bool>? onSuccess;
  final ValueChanged? onFailure;

  const SignIn(
      {Key? key,
      this.primaryColor = const Color(0xFF131B22),
      this.secondaryColor = const Color(0xFF6727BE),
      this.textColor = const Color(0xFFFFFFFF),
      this.onSuccess,
      this.onFailure})
      : super(key: key);

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  AssetImage moiIdLogo = const AssetImage('https://moi-id.life/assets/logo.png');
  final GlobalKey<FormState> _key = GlobalKey();
  late String username, password, userID;
  bool obscured = true;

  void _toggleObscured() {
    setState(() {
      obscured = !obscured; // Prevents focus if tap on eye
    });
  }

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      WebView.platform = SurfaceAndroidWebView();
    }
  }

  void onSuccessHandler(String userID, bool authStatus) {
    if (widget.onSuccess != null) {
      widget.onSuccess!(userID, authStatus);
    }
  }

  void onFailure(e) {
    if (widget.onFailure != null) {
      widget.onFailure!(e);
    }
  }

  Widget loginButton() {
    return FutureBuilder<WebViewController>(
        future: _controller.future,
        builder: (BuildContext context,
            AsyncSnapshot<WebViewController> controller) {
          return ElevatedButton(
            onPressed: () async {
              try {
                if (_key.currentState!.validate()) {
                  userID = await getUserID(username);
                  final userZKProof = await getZkProofOfUser(userID, "zk");

                  var _con = controller.data!;
                  await _con.runJavascript('''
                      var userZKP = ($userZKProof)['_proof'];
                      var validateJSON = window.hckzp.validateZkp(JSON.stringify(userZKP),"Test@321");
                      IomeChannel.postMessage(JSON.stringify(validateJSON));
                  ''');
                }
              } catch (e) {
                // onFailure()
              }
            },
            child: Text('SUBMIT',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: widget.textColor)),
            style: ElevatedButton.styleFrom(
                primary: widget.secondaryColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0))),
          );
        });
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'IomeChannel',
        onMessageReceived: (JavascriptMessage message) {
          final Map<String, dynamic> ksToken = jsonDecode(message.message);
          if (ksToken['authStatus']) {
            // print(ksToken['authToken']); // used to read keystore
            onSuccessHandler(userID, true);
          } else {
            onSuccessHandler("na", false);
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: widget.primaryColor,
        body: Form(
            key: _key,
            child: Column(
              children: <Widget>[
                const Padding(padding: EdgeInsets.all(10.0)),
                Container(
                    margin: const EdgeInsets.only(top: 100.0, bottom: 32.0),
                    child: const Text("SIGN IN",
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.bold))),
                ListTile(
                    title: TextFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please enter your username";
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "Username"),
                  onChanged: (value1) {
                    username = value1;
                  },
                )),
                Container(margin: const EdgeInsets.only(top: 16.0)),
                ListTile(
                    title: TextFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please enter your password";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      labelText: "Password",
                      suffixIcon: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 4, 0),
                        child: GestureDetector(
                          onTap: _toggleObscured,
                          child: Icon(
                            obscured
                                ? Icons.visibility_off_rounded
                                : Icons.visibility_rounded,
                            size: 24,
                          ),
                        ),
                      )),
                  obscureText: obscured,
                  onChanged: (value) {
                    password = value;
                  },
                )),
                Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 16.0),
                    padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                    child: Text("Forgot Password?",
                        style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.w700,
                            color: widget.textColor))),
                Container(
                    margin: const EdgeInsets.only(top: 21.0),
                    padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                    height: 48.0,
                    width: double.infinity,
                    child: loginButton()),
                Container(margin: const EdgeInsets.only(top: 48.0)),
                RichText(
                    text: TextSpan(
                        children: const <TextSpan>[
                      TextSpan(text: "Don't have an account? "),
                      TextSpan(
                          text: "Register here",
                          style:
                              TextStyle(decoration: TextDecoration.underline))
                    ],
                        style: TextStyle(
                            color: widget.textColor,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w700))),
                const Spacer(),
                Container(
                    margin: const EdgeInsets.only(bottom: 48.0),
                    width: double.infinity,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          const Text("POWERED BY ",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.w700)),
                          Image(image: moiIdLogo, height: 64.0, width: 64.0)
                        ])),
                Visibility(
                  visible: false,
                  maintainState: true,
                  child: SizedBox(
                      height: 1,
                      child: WebView(
                          initialUrl: 'https://zkp.netlify.app',
                          javascriptMode: JavascriptMode.unrestricted,
                          onWebViewCreated:
                              (WebViewController webViewController) {
                            _controller.complete(webViewController);
                          },
                          // onProgress: (int progress) {
                          //   print('WebView is loading (progress : $progress%)');
                          // },
                          javascriptChannels: <JavascriptChannel>{
                            _toasterJavascriptChannel(context),
                          },
                          // onPageStarted: (String url) {
                          //   print('Page started loading: $url');
                          // },
                          // onPageFinished: (String url) {
                          //   print('Page finished loading: $url');
                          // },
                          gestureNavigationEnabled: true,
                          backgroundColor: const Color(0x00000000))),
                ),
              ],
            )));
  }
}
